package util;

public interface ObserverZmenyProstoru {
    /**
     * Metoda, ve které proběhne aktualizace pozorovatele,
     * je volána prostřednictvím metody upozorniPozorovatele z rozhraní SubjektZmenyProstoru
     *
     */
    void aktualizuj();

}
