package util;

public interface ObserverZmenyInventare {
    /**
     * Metoda, ve které proběhne aktualizace pozorovatele,
     * je volána prostřednictvím metody upozorniPozorovatele z rozhraní SubjektZmenyInventare
     *
     */
    void aktualizuj();
}
