package util;

public interface SubjektZmenyInventare {
    /**
     * Metoda slouží k zaregistrování pozorovatele, musí to být instance třídy,
     *  která implementuje rozhraní ObserverZmenyInventare.
     *
     * @param pozorovatel registrovaný objekt
     */
    void zaregistrujPozorovateleInventare(ObserverZmenyInventare pozorovatel);

    /**
     * Metoda slouží k zrušení registrace pozorovatele, musí to být instance třídy,
     *  která implementuje rozhraní ObserverZmenyInventare.
     *
     * @param pozorovatel objekt, který již nechce být informován o změnách
     */
    void odregistrujPozorovateleInventare(ObserverZmenyInventare pozorovatel);


    /**
     * Metoda, která se volá vždy, když dojde ke změně této instance.
     * Upozorní všechny pozorovatele, že došlo ke změně tak, že zavolá jejich metodu aktualizuj.
     */
    void upozorniPozorovateleInventare();
}
