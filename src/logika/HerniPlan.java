package logika;

import util.ObserverZmenyInventare;
import util.ObserverZmenyProstoru;
import util.SubjektZmenyInventare;
import util.SubjektZmenyProstoru;

import java.util.ArrayList;
import java.util.List;

/**
 *  Class logika.HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */
public class HerniPlan implements SubjektZmenyProstoru, SubjektZmenyInventare {
    
    private Prostor aktualniProstor;
    private boolean vyhra = false; 
    private boolean prohra = false; 
    private Inventar inventar;
    private List<ObserverZmenyProstoru> seznamPozorovatelu ;
    private List<ObserverZmenyInventare> seznamPozorovateluInventare;
    private HerniPlan plan;


    /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
     HerniPlan() {
        zalozProstoryHry();
        inventar = new Inventar(plan);
        seznamPozorovatelu = new ArrayList<>();
        seznamPozorovateluInventare = new ArrayList<>();

     }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        Prostor hrad = new Prostor("hrad","hrad, ve kterém bydlí král", 300, 150);
        Prostor les = new Prostor("les", "velký, tajemný a neprobádaný les", 190, 175);
        Prostor kovarna = new Prostor("kovarna","dům jediného kováře v tomto království",170, 90);
        Prostor domecek = new Prostor("domecek","domeček staré babky kořenářky",110, 100);
        Prostor louka = new Prostor ("louka", "louka se spoustou lučního kvítí", 50, 50);
        Prostor jeskyne = new Prostor("jeskyne","stará a velmi tmavá jeskyně", 70, 180);
        Prostor jeskynepoklad = new Prostor ("jeskynepoklad", "skrytá část jeskyně, kde je ukrytý poklad",50, 200);
        
        domecek.setVychod(les);
        domecek.setVychod(louka);
        les.setVychod(domecek);
        les.setVychod(kovarna);
        les.setVychod(hrad);
        les.setVychod(jeskyne);
        kovarna.setVychod(les);
        louka.setVychod(domecek);
        hrad.setVychod(les);
        jeskyne.setVychod(les);
        jeskyne.setVychod(jeskynepoklad);
        jeskynepoklad.setVychod(jeskyne);
        
        jeskyne.budeVidet(false);
        
        Vec kviti = new Vec("kviti", true);
        Vec koreny = new Vec("koreny", true);
        Vec vetev = new Vec("vetev", true);
        Vec tupymec = new Vec("tupymec", false);
        Vec pochoden = new Vec ("pochoden", false);
        Vec vyhen = new Vec("vyhen", false);
        Vec postel = new Vec("postel", false);
        Vec poklad = new Vec("poklad", true);
        Vec truhla = new Vec("truhla", false);

        truhla.vlozVec(new Vec("mec",true));
        truhla.prozkoumano(false);
        
        louka.vlozVec(kviti);
        les.vlozVec(koreny);
        les.vlozVec(vetev);
        kovarna.vlozVec(tupymec);
        kovarna.vlozVec(pochoden);
        kovarna.vlozVec(vyhen);
        kovarna.vlozVec(truhla);
        domecek.vlozVec(postel);
        jeskynepoklad.vlozVec(poklad);
        
        hrad.vlozPostavu(new Postava ("kral", "Vrať mi poklad, jsi má jediná naděje! Kovář ti dá meč."));
        kovarna.vlozPostavu(new Postava ("kovar", "Když mi přineseš z lesa větev, udělám ti z ní pochodeň!"));
        domecek.vlozPostavu(new Postava ("babka", "Slyšela jsem o tvém úkolu, dones mi kořeny z lesa a kvítí z louky a pomůžu ti s tvým úkolem!"));
        jeskyne.vlozPostavu(new Postava ("bandita", "Uááááááá"));
        
        aktualniProstor = hrad;
    }
        
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */

    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    void setAktualniProstor(Prostor prostor) {
        aktualniProstor = prostor;
        upozorniPozorovatele();
        upozorniPozorovateleInventare();


    }
    
    /**
     * Jestli je prostor vítězný.
     */
    boolean jeVyhra(){
        return vyhra;
    }
     
    /**
     * Nastaví výhru.
     */
    void setVyhra(boolean stav) {
        this.vyhra = stav;
    }
    /**
     * Jestli prostor spustí prohru.
     */
    boolean jeProhra() {
        return prohra;
    }
    
    /**
     * Nastaví prohru
     */
    void setProhra() {
        this.prohra = true;
    }
    
    /**
     *  Metoda vrací inventář.
     *
     *@return inventar s věcmi, které má postava
     */
    public Inventar getInventar() {
        return inventar;
    }

    /**
     * Metoda slouží k zaregistrování pozorovatele, musí to být instance třídy,
     *  která implementuje rozhraní ObserverZmenyProstoru.
     *
     * @param pozorovatel registrovaný objekt
     */
    @Override
    public void zaregistrujPozorovatele(ObserverZmenyProstoru pozorovatel) {
        seznamPozorovatelu.add(pozorovatel);
    }

    /**
     * Metoda slouží k zrušení registrace pozorovatele, musí to být instance třídy,
     *  která implementuje rozhraní ObserverZmenyProstoru.
     *
     * @param pozorovatel objekt, který již nechce být informován o změnách
     */
    @Override
    public void odregistrujPozorovatele(ObserverZmenyProstoru pozorovatel) {
        seznamPozorovatelu.remove(pozorovatel);
    }

    /**
     * Metoda, která se volá vždy, když dojde ke změně této instance.
     * Upozorní všechny pozorovatele, že došlo ke změně tak, že zavolá jejich metodu aktualizuj.
     */
    @Override
    public void upozorniPozorovatele() {
        for (ObserverZmenyProstoru pozorovatel :seznamPozorovatelu){
            pozorovatel.aktualizuj();
        }

    }

    /**
     * Metoda slouží k zaregistrování pozorovatele, musí to být instance třídy,
     *  která implementuje rozhraní ObserverZmenyInventare.
     *
     * @param pozorovatel registrovaný objekt
     */
    @Override
    public void zaregistrujPozorovateleInventare(ObserverZmenyInventare pozorovatel) {
        seznamPozorovateluInventare.add(pozorovatel);
    }

    /**
     * Metoda slouží k zrušení registrace pozorovatele, musí to být instance třídy,
     *  která implementuje rozhraní ObserverZmenyInventare.
     *
     * @param pozorovatel objekt, který již nechce být informován o změnách
     */
    @Override
    public void odregistrujPozorovateleInventare(ObserverZmenyInventare pozorovatel) {
        seznamPozorovateluInventare.remove(pozorovatel);
    }

    /**
     * Metoda, která se volá vždy, když dojde ke změně této instance.
     * Upozorní všechny pozorovatele, že došlo ke změně tak, že zavolá jejich metodu aktualizuj.
     */
    @Override
    public void upozorniPozorovateleInventare() {
        for (ObserverZmenyInventare pozorovatel :seznamPozorovateluInventare){
            pozorovatel.aktualizuj();
        }
    }
}
