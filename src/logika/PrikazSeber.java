package logika;

/*******************************************************************************
 * Třída logika.PrikazSeber implementuje pro hru příkaz seber.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 * @author    Petr Mikan
 * @version   školní rok 2017/2018
 */
public class PrikazSeber implements IPrikaz
{
    //== Datové atributy (statické i instancí)======================================
    private static final String NAZEV = "seber";
    private HerniPlan plan;
    //== Konstruktory a tovární metody =============================================

    /**
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
     */
    PrikazSeber(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     *  Provádí příkaz "seber". Nejdřív hledá sbíranou věc v prostoru, pak jí
     *  sebere a vloží do batohu (pokud je přenositelná a v batohu je místo).
     *
     *@param parametry - jako  parametr obsahuje název sbírané věci
     *@return zpráva, kterou vypíše hra hráči
     */     
    @Override
    public String provedPrikaz(String... parametry) {
        if(parametry.length == 0) {
            return "Co mám sebrat? Zadej název věci.";
        }
        String nazevSbiraneVeci = parametry[0];
        Prostor aktualniProstor = plan.getAktualniProstor();
        if (aktualniProstor.obsahujeVec(nazevSbiraneVeci)){
            Vec sbiranaVec = aktualniProstor.vyberVec(nazevSbiraneVeci);
            if (sbiranaVec == null || !sbiranaVec.jePrenositelna()) {  
                return "Toto sebrat nemůžeš.";
            }
            if (aktualniProstor.getNazev().equals("jeskynepoklad") && sbiranaVec.getNazev().equals("poklad")) {
                plan.setVyhra(true);
                plan.getInventar().vyhodVec("mec");
                plan.getInventar().vlozVec(sbiranaVec);
                plan.upozorniPozorovateleInventare();
                return "Výhra! \n";
            }
            else {
                if (sbiranaVec.jePrenositelna()) {
                    plan.getInventar().vlozVec(sbiranaVec);
                    plan.getAktualniProstor().odeberVec(nazevSbiraneVeci);
                    plan.upozorniPozorovateleInventare();
                    return "Sebral jsi věc: " + sbiranaVec.getNazev() + 
                           " a vložil sis tuto věc do inventáře.";
                }                             
                else {
                     aktualniProstor.vlozVec(sbiranaVec);                       
                     return "Inventář již máš plný.";
                }
            }
        }
        else {
            return "To opravdu neseberu!";
        }
    }
       
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}
