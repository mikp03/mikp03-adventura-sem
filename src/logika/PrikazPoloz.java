package logika;

public class PrikazPoloz implements IPrikaz
{
    //== Datové atributy (statické i instancí)======================================
    private static final String NAZEV = "poloz";
    private HerniPlan plan;
    //== Konstruktory a tovární metody =============================================

    /**
     *  Konstruktor třídy
     *
     *  @param plan herní plán, ve kterém se bude ve hře "chodit"
     */
    PrikazPoloz(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     *  Provádí příkaz "poloz". Nejdřív hledá sbíranou věc v prostoru, pak jí
     *  sebere a vloží do batohu (pokud je přenositelná a v batohu je místo).
     *
     *@param parametry - jako  parametr obsahuje název pokládané věci
     *@return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if(parametry.length != 2) {
            return "Co mám položit? Příkaz vypadá 'poloz něco někam'.";
        }
        String nazevPokladaneVeci = parametry[0];
        String nazevTruhly = parametry[1];
        if (plan.getInventar().obsahujeVec(nazevPokladaneVeci) && plan.getAktualniProstor().getNazev().equals("kovarna")) {
            plan.getInventar().vyhodVec(nazevPokladaneVeci);
            Vec vec = plan.getInventar().getVec(nazevPokladaneVeci);
//            plan.getAktualniProstor().getVec(nazevTruhly).vlozVec(vec);
            Vec truhla = plan.getAktualniProstor().getVec(nazevTruhly);
            truhla.vlozVec(new Vec(nazevPokladaneVeci, true));
            plan.upozorniPozorovateleInventare();
            return "Vložil jsi "+ nazevPokladaneVeci +" do truhly.";
        }
        return "Pokládat můžeš pouze do truhly a věci, které máš u sebe!";
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}
