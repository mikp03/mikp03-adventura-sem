package logika;

/*******************************************************************************
 * Třída logika.PrikazBojuj implementuje pro hru příkaz bojuj.
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author    Petr Mikan
 * @version   pro školní rok 2017/2018
 */
public class PrikazBojuj implements IPrikaz {
    
    private static final String NAZEV = "bojuj";
    private HerniPlan plan;
    private Inventar inventar;
    
    /**
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, ve kterém se bude ve hře "chodit"
     */
    PrikazBojuj(HerniPlan plan)
    {
        this.plan = plan;
        this.inventar = plan.getInventar();
    }

    /**
     * Provádí příkaz "bojuj". Pokud je daná osoba v prostoru, zkontroluje se, zda-li má postava meč a pochodeň.
     * Pokud má obojí, souboj vyhraje.
     * 
     *@param parametry - jako  parametr obsahuje jméno osoby,
     *                         s kterou se má bojovat.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry){   
        if (parametry.length == 0) {
            return "S kým mám bojovat?";
        }
        
        Prostor aktualniProstor = plan.getAktualniProstor();
        
        if ( aktualniProstor.getNazev().equals("jeskyne")) {
            if (inventar.obsahujeVec("mec") && inventar.obsahujeVec("pochoden")) {
                aktualniProstor.vratSousedniProstor("jeskynepoklad").jeVidet();
                aktualniProstor.odeberPostavu("bandita");
                return "Porazil jsi náčelníka banditů kpt. Kormoranta!\n"+
                        "V dáli vidíš něco lesklého.";
            }
            else {               
                plan.setProhra();
                return "Byl jsi zabit.";
            }
        }
        return "Ten s tebou nechce bojovat!";
    }

    
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}
