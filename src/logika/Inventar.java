package logika;

import util.ObserverZmenyInventare;
import util.SubjektZmenyInventare;

import java.util.*;

/*******************************************************************************
 * Třída logika.Inventar popisuje inventář postavy v adventuře.
 *
 * @author    Petr Mikan
 * @version   pro školní rok 2017/2018
 */
public class Inventar implements SubjektZmenyInventare
{
    private HerniPlan plan;
    private static final int KAPACITA = 5;
    private Map<String, Vec> veci;
    private List<ObserverZmenyInventare> seznamPozorovateluInventare;

    /**
     * Konstruktor třídy
     * @param plan herní plán
     */
    Inventar(HerniPlan plan) {
        this.plan = plan;
        veci = new HashMap<>();
        seznamPozorovateluInventare = new ArrayList<>();
    }

    /**
    * Pokud se tam vejde, vloží věc do inventáře.
    * 
    * @param vec třídy logika.Vec.
    */
    void vlozVec(Vec vec) {
        if (veci.size() < KAPACITA ) {
            veci.put(vec.getNazev(), vec);
        }
    }

//    /**
//     * Metoda položí věc z inventáře do aktuálního prostoru.
//     *
//     * @param  jmenoVeci String jméno věci, kterou chceme položit.
//     */
//    void polozVec(String jmenoVeci){
//        Vec vec = getVec(jmenoVeci);
//        Vec truhla = plan.getAktualniProstor().getVec("truhla");
//        truhla.vlozVec(vec);
//        veci.remove(jmenoVeci);
//    }
    
    /**
     * Metoda vyhodí věc z inventáře.
     *
     * @param  jmenoVeci String jméno věci, kterou chceme vyhodit.
     */
    void vyhodVec(String jmenoVeci){
        veci.remove(jmenoVeci);
    }
    
    /**
     *  Metoda zjistí jméno věci z inventáře.
     *  
     *  @param  nazevVeci  String názvu věci, kterou chceme najít.
     */

    Vec getVec(String nazevVeci) {
        return veci.get(nazevVeci);
    }

//    /**
//     * Metoda zjistí, jestli je inventář plný.
//     *
//     * @return boolean - true, pokud je plný.
//     */
//    public boolean jePlny(){
//        return (veci.size() < KAPACITA);
//    }
    
    /**
    * Napíše, zda-li se věc nachází v inventáři.
    * 
    * @return boolean - true, pokud ano.
     */
    boolean obsahujeVec(String jmenoVeci) {
        return this.veci.containsKey(jmenoVeci);
    }

    /**
     * Vypíše věci, které jsou v inventáři.
     * 
     * @return vrací seznam věcí v inventáři.
     */
    String nazvyVeci() {
        StringBuilder nazvy = new StringBuilder("Věci v inventáři: ");
        for (String jmenoVeci : veci.keySet()) {
            nazvy.append(jmenoVeci).append(" ");
        }
        return nazvy.toString();
    }

//    /**
//     * Vypíše kapacitu inventáře.
//     *
//     * @return int hodnotu kapacity inventáře.
//     */
//    public int getKapacita() {
//        return KAPACITA;
//
//    }
//
//    public String getVeci() {
//        StringBuilder text = new StringBuilder();
//        for (String nazev : veci.keySet()) {
//            text.append(nazev).append(" ");
//        }
//        return text.toString();
//    }

    /**
     * Metoda vracející seznam predmětů
     * @return  seznam předmětů
     */

    public Set<Vec> seznamPredmetu () {
        Set<Vec> veciVInventari = new HashSet<>();
        for (String nazev : veci.keySet()) {
            Vec v = veci.get(nazev);
            veciVInventari.add(v);
        }
        return veciVInventari;
    }

    @Override
    public void zaregistrujPozorovateleInventare(ObserverZmenyInventare pozorovatel) {
        seznamPozorovateluInventare.add(pozorovatel);
    }

    @Override
    public void odregistrujPozorovateleInventare(ObserverZmenyInventare pozorovatel) {
        seznamPozorovateluInventare.remove(pozorovatel);
    }

    @Override
    public void upozorniPozorovateleInventare() {
        for (ObserverZmenyInventare pozorovatel : seznamPozorovateluInventare) {
            pozorovatel.aktualizuj();
        }
    }
}
