package logika;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/*******************************************************************************
 * Třída logika.Vec popisuje jednotlivé věci v adventuře.
 *  Tato třída je součástí jednoduché textové hry.
 *
 * @author    Petr Mikan
 * @version   pro školní rok 2017/2018
 */
public class Vec
{
    private String nazev;
    private boolean prenositelnost;
    private Map<String,Vec> veci;
    private boolean prozkoumana = false;

    /***************************************************************************
     *  Konstruktor třídy
     */
    Vec(String nazev, boolean prenositelnost)
    {
        this.nazev = nazev;
        this.prenositelnost = prenositelnost;
        veci = new HashMap<>();
    }

    /**
     * Metoda vracející seznam predmětů
     * @return  seznam předmětů
     */

    public Set<Vec> seznamPredmetu () {
        Set<Vec> veciVProstoru = new HashSet<>();
        for (String nazev : veci.keySet()) {
            Vec v = veci.get(nazev);
            veciVProstoru.add(v);
        }
        return veciVProstoru;
    }


    /**
     * Metoda zjistí název věci.
     * 
     * @return název věci
     */
    public String getNazev(){
        return nazev;
    }
    /**
     * Metoda zjistí, zda-li je věc přenositelná.
     * 
     * @return boolean - true, pokud je přenositelná
     */
    boolean jePrenositelna(){
        return prenositelnost;
    }

    boolean jeProzkoumana() {
        return prozkoumana;
    }

    void prozkoumano(boolean prozkoumana) {
        this.prozkoumana = prozkoumana;
    }

    void vlozVec(Vec neco) {
        veci.put(neco.getNazev(),neco);
    }

    boolean obsahujeVec(String jmeno) {
        return prozkoumana && veci.containsKey(jmeno);
    }

    Vec vyberVec(String jmeno) {
        Vec vec = null;
        if (prozkoumana && veci.containsKey(jmeno)) {
            vec = veci.get(jmeno);
            if (vec.jePrenositelna()) {
                veci.remove(jmeno);
            }
        }
        return vec;
    }

    String popisProzkoumej() {
        if (veci.isEmpty()) {
            return "prohlédl jsi si pozorně "+nazev+", je opravdu dobře udělaný";
        }
        StringBuilder popis = new StringBuilder("Prohlédl jsi si pozorně " + nazev + " a uvnitř jsi našel:");
        for (String jmeno : veci.keySet()) {
            popis.append(" ").append(jmeno);
        }
        return popis.toString();
    }

}
