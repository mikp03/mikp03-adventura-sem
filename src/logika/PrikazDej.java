package logika;

class PrikazDej implements IPrikaz
{
    private static final String NAZEV = "dej";
    private HerniPlan plan;
    private Inventar inventar;

    PrikazDej(final HerniPlan plan) {
        this.plan = plan;
        this.inventar = plan.getInventar();
    }

    @Override
    public String provedPrikaz(final String... parametry) {
        if (parametry.length == 0) {
            return "Nev\u00edm, co komu m\u00e1m d\u00e1t. Mus\u00ed\u0161 uv\u00e9st n\u00e1zev v\u011bci!\nP\u0159\u00edkaz m\u00e1 vypadat: dej postava v\u011bc";
        }
        final String postava = parametry[0];
        final String vec = parametry[1];
        final Prostor aktualniProstor = this.plan.getAktualniProstor();
        final Vec davana = this.inventar.getVec(vec);
        final Postava jmeno = aktualniProstor.najdiPostavu(postava);
        if (davana == null) {
            return "Tuto v\u011bc nem\u00e1\u0161 v invent\u00e1\u0159i";
        }
        if (jmeno == null) {
            return "Tato postava zde nen\u00ed";
        }
        if (davana.getNazev().equals("kviti") || (davana.getNazev().equals("koreny") && jmeno.getJmeno().equals("babka") && this.inventar.obsahujeVec("kviti") && this.inventar.obsahujeVec("koreny"))) {
            this.plan.getAktualniProstor().vratSousedniProstor("les").vratSousedniProstor("jeskyne").budeVidet(true);
            this.inventar.vyhodVec("koreny");
            this.inventar.vyhodVec("kviti");
            plan.upozorniPozorovateleInventare();
            return "Vypad\u00e1, jako by mi cht\u011bla je\u0161t\u011b n\u011bco \u0159\u00edct.";
        }
        if (davana.getNazev().equals("vetev") && aktualniProstor.getNazev().equals("kovarna")) {
            this.inventar.vyhodVec("vetev");
            this.inventar.vlozVec(new Vec("pochoden", true));
            plan.upozorniPozorovateleInventare();
            return "Tady m\u00e1\u0161 pochode\u0148, ur\u010dit\u011b se ti bude hodit!";
        }
        return "Tohle postava nechce";
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}