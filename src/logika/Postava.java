package logika;

/*******************************************************************************
 * Třída logika.Postava popisuje jednotlivé postavy v adventuře.
 *  Tato třída je součástí jednoduché textové hry.
 *
 * @author    Petr Mikan
 * @version   pro školní rok 2017/2018
 */
class Postava
{
    private String jmeno;
    private String dialog;

    /**
     *  Konstruktor třídy
     *  
     *  @param jmeno Jméno postavy v adventuře.
     *  @param dialog Dialog postavy v adventuře.
     */
    Postava(String jmeno, String dialog) {
        this.jmeno = jmeno;
        this.dialog = dialog;
    }

    /**
     * Metoda zjistí jméno postavy.
     * 
     * @return   vrací jméno postavy.
     */
    String getJmeno() {
        return jmeno; 
    }
    
    /**
     * Metoda zjistí dialog postavy.
     * 
     * @return  vrací dialog postavy.
     */
    String getDialog() {
        return dialog;
    }
}
