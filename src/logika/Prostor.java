package logika;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

/**
 * Trida logika.Prostor - popisuje jednotlivé prostory (místnosti) hry
 *
 * Tato třída je součástí jednoduché textové hry.
 *
 * "logika.Prostor" reprezentuje jedno místo (místnost, prostor, ..) ve scénáři hry.
 * logika.Prostor může mít sousední prostory připojené přes východy. Pro každý východ
 * si prostor ukládá odkaz na sousedící prostor.
 *
 * @author Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 * @version pro školní rok 2016/2017
 */
public class Prostor {

    private String nazev;
    private String popis;
    private boolean jeVidet = true;
    private Set<Prostor> vychody;
    private Map<String, Vec> veci;
    private Map<String, Postava> postavy;
    private double posLeft;
    private double posTop;

    /**
     * Konstruktor třídy
     *
     * @param nazev Název prostoru.
     * @param popis Popis prostoru.
     */
    Prostor(String nazev, String popis, double posLeft, double posTop) {
        this.nazev = nazev;
        this.popis = popis;
        vychody = new HashSet<>();
        veci = new HashMap<>();
        postavy = new HashMap<>();
        this.posLeft = posLeft;
        this.posTop = posTop;
    }

    /**
     * Definuje východ z prostoru (sousední/vedlejsi prostor). Vzhledem k tomu,
     * že je použit Set pro uložení východů, může být sousední prostor uveden
     * pouze jednou (tj. nelze mít dvoje dveře do stejné sousední místnosti).
     * Druhé zadání stejného prostoru tiše přepíše předchozí zadání (neobjeví se
     * žádné chybové hlášení). Lze zadat též cestu ze do sebe sama.
     *
     * @param vedlejsi prostor, který sousedi s aktualnim prostorem.
     *
     */
    void setVychod(Prostor vedlejsi) {
        vychody.add(vedlejsi);
    }

    /**
     * Metoda equals pro porovnání dvou prostorů. Překrývá se metoda equals ze
     * třídy Object. Dva prostory jsou shodné, pokud mají stejný název. Tato
     * metoda je důležitá z hlediska správného fungování seznamu východů (Set).
     *
     * Bližší popis metody equals je u třídy Object.
     *
     * @param o object, který se má porovnávat s aktuálním
     * @return hodnotu true, pokud má zadaný prostor stejný název, jinak false
     */  
      @Override
    public boolean equals(Object o) {
        // porovnáváme zda se nejedná o dva odkazy na stejnou instanci
        if (this == o) {
            return true;
        }
        // porovnáváme jakého typu je parametr 
        if (!(o instanceof Prostor)) {
            return false;    // pokud parametr není typu logika.Prostor, vrátíme false
        }
        // přetypujeme parametr na typ logika.Prostor
        Prostor druhy = (Prostor) o;

        //metoda equals třídy java.util.Objects porovná hodnoty obou názvů. 
        //Vrátí true pro stejné názvy a i v případě, že jsou oba názvy null,
        //jinak vrátí false.

       return (java.util.Objects.equals(this.nazev, druhy.nazev));       
    }

    /**
     * metoda hashCode vraci ciselny identifikator instance, ktery se pouziva
     * pro optimalizaci ukladani v dynamickych datovych strukturach. Pri
     * prekryti metody equals je potreba prekryt i metodu hashCode. Podrobny
     * popis pravidel pro vytvareni metody hashCode je u metody hashCode ve
     * tride Object
     */
    @Override
    public int hashCode() {
        int vysledek = 3;
        int hashNazvu = java.util.Objects.hashCode(this.nazev);
        vysledek = 37 * vysledek + hashNazvu;
        return vysledek;
    }
      

    /**
     * Vrací název prostoru (byl zadán při vytváření prostoru jako parametr
     * konstruktoru)
     *
     * @return název prostoru
     */
    public String getNazev() {
        return nazev;       
    }
    
    /**
     * Vrací "dlouhý" popis prostoru, který může vypadat následovně: Jsi v
     * mistnosti/prostoru vstupni hala budovy VSE na Jiznim meste. vychody:
     * chodba bufet ucebna
     *
     * @return Dlouhý popis prostoru
     */
    String dlouhyPopis() {
        for (Vec vec : seznamPredmetu()) {
                vec.prozkoumano(true);
        }
        return "Jsi v mistnosti/prostoru " + popis + ".\n"
                + popisVychodu() +"\n"
                + getVeci() +"\n"
                + getPostavy();
    }

    /**
     * Vrací textový řetězec, který popisuje sousední východy, například:
     * "vychody: hala ".
     *
     * @return Popis východů - názvů sousedních prostorů
     */
    private String popisVychodu() {
        StringBuilder vracenyText = new StringBuilder("východy:");
        for (Prostor sousedni : vychody) {
            if (sousedni.jeVidet()) {
                 vracenyText.append(" ").append(sousedni.getNazev());
            }
        }
        return vracenyText.toString();
    }
    
    /**
     * Vrací prostor, který sousedí s aktuálním prostorem a jehož název je zadán
     * jako parametr. Pokud prostor s udaným jménem nesousedí s aktuálním
     * prostorem, vrací se hodnota null.
     *
     * @param nazevSouseda Jméno sousedního prostoru (východu)
     * @return logika.Prostor, který se nachází za příslušným východem, nebo hodnota
     * null, pokud prostor zadaného jména není sousedem.
     */
    Prostor vratSousedniProstor(String nazevSouseda) {
        if (nazevSouseda == null) {
            return null;
        }
        for (Prostor sousedni : vychody) {
            if (sousedni.getNazev().equals(nazevSouseda)) {
                return sousedni;
            }
        }
        return null;
    }

    /**
     * Vrací kolekci obsahující prostory, se kterými tento prostor sousedí.
     * Takto získaný seznam sousedních prostor nelze upravovat (přidávat,
     * odebírat východy) protože z hlediska správného návrhu je to plně
     * záležitostí třídy logika.Prostor.
     *
     * @return Nemodifikovatelná kolekce prostorů (východů), se kterými tento
     * prostor sousedí.
     */
    public Collection<Prostor> getVychody() {
        return Collections.unmodifiableCollection(vychody);
    }
    
    /**
     * Metoda vloží věc.
     * 
     * @return boolean - true, pokud se to povede.
     */
    boolean vlozVec(Vec neco){
        if(veci.containsKey(neco.getNazev())) {
            return false;
        }
        else{
            veci.put(neco.getNazev(),neco);
            return true;
        }
    }
    
    /**
     * Metoda najde věc.
     * 
     * @return vrátí nalezenou věc v prostoru.
     */
    Vec najdiVec(String nazevVeci){
        return veci.get(nazevVeci);
    }
    
    /**
     * Odebere věc.
     * 
     * @param jmeno třídy logika.Vec
     */
    Vec odeberVec(String jmeno) {
        return veci.remove(jmeno);
    }

    /**
     * Vrací seznam věcí z prostoru.
     * 
     * @return  vrací názvy věcí.
     */

    private String getVeci() {
        StringBuilder nazvy = new StringBuilder("  jsou zde tyto věci: ");
        for (String jmenoVeci : veci.keySet()){
            nazvy.append(jmenoVeci).append(" ");
        }
        return nazvy.toString();
    }

    /**
     * Metoda vracející seznam predmětů
     * @return  seznam předmětů
     */

    public Set<Vec> seznamPredmetu () {
        Set<Vec> veciVProstoru = new HashSet<>();
        for (String nazev : veci.keySet()) {
            Vec v = veci.get(nazev);
            veciVProstoru.add(v);
        }
        return veciVProstoru;
    }


    /**
     * Metoda vrací seznam postav z prostoru.
     * 
     * return  vrací seznam postav.
     */

    private String getPostavy() {
        StringBuilder nazvy = new StringBuilder("  jsou zde tyto postavy: ");
        for (String jmenoPostav : postavy.keySet()){
            nazvy.append(jmenoPostav).append(" ");
        }
        return nazvy.toString();
    }
    
    /**
     * Metoda, zda-li se nachází věc v prostoru.
     * 
     * @param jmeno String jméno věci
     * 
     * @return boolean - true, pokud ano.
     */



    boolean obsahujeVec(String jmeno) {
        if (najdiVecVMistnosti(jmeno) == null) {
        for (Vec vec : veci.values()) {
            if (vec.obsahujeVec(jmeno)) {
                return true;
            }
        }
        return false;
        }
        else {
            return true;
        }
    }

    Vec getVec(String jmenoVeci) {
        return veci.get(jmenoVeci);
    }

    Vec vyberVec(String jmeno) {
        Vec vybranaVec = najdiVecVMistnosti(jmeno);
        if (vybranaVec != null && vybranaVec.jePrenositelna()) {
            veci.remove(vybranaVec);
        }
        else {
            for (Vec vec : veci.values()) { // hledám, zda není v nějaké věci
                vybranaVec=vec.vyberVec(jmeno);
                if (vybranaVec != null) {
                    break;
                }
            }
        }
        return vybranaVec;
    }

    private Vec najdiVecVMistnosti(String jmeno) {
        Vec vec = null;
        for (Vec neco : veci.values()) {
            if (neco.getNazev().equals(jmeno)) {
                vec = neco;
                break;
            }
        }
        return vec;
    }
    
    /**
     * Vloží postavu do prostoru.
     * 
     * @param postava třída logika.Postava.
     * 
     * @return boolean - true, pokud se v prostoru nachází.
     */

    boolean vlozPostavu(Postava postava)
    {
        if(postavy.containsKey(postava.getJmeno())) {
            return false;
        }
        else{
            postavy.put(postava.getJmeno(),postava);
            return true;
        }
    }

    /**
     * Metoda najde postavu.
     * 
     * @param jmeno String jméno postavy
     */

    Postava najdiPostavu(String jmeno)
    {
        return postavy.get(jmeno);
    }
    
    /**
     * Odebere postavu.
     * 
     * @param jmeno String jméno postavy
     */
    Postava odeberPostavu(String jmeno){
        return postavy.remove(jmeno);
    }

    /**
     * Metoda, zda-li se nachází postava v prostoru.
     *
     * @param jmeno String jméno postavy
     *
     * @return boolean - true, pokud ano.
     */


    boolean obsahujePostavu(String jmeno) {
        for (String nazev : postavy.keySet()) {
            if (nazev.equals(jmeno)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Nastaví viditelnost.
     * 
     * @param viditelna boolean, jestli je mísntost viditelná
     */
    void budeVidet(boolean viditelna) {
        this.jeVidet = viditelna;
    }
    
    /**
     * Zjistí, jestli je místnost viditelná.
     * 
     * @return boolean - true, pokud je viditelná.
     */
    public boolean jeVidet() {
        return jeVidet;
    }

    public double getPosLeft() {
        return posLeft;
    }

    public double getPosTop() {
        return posTop;
    }
}
