package logika;

class PrikazMluv implements IPrikaz
{
    private static final String NAZEV = "mluv";
    private HerniPlan plan;

    PrikazMluv(final HerniPlan plan) {
        this.plan = plan;
    }

    @Override
    public String provedPrikaz(final String... parametry) {
        if (parametry.length == 0) {
            return "S k\u00fdm m\u00e1m hovo\u0159it?";
        }
        final String jmeno = parametry[0];
        final Prostor aktualniProstor = this.plan.getAktualniProstor();
        final Postava postava = aktualniProstor.najdiPostavu(jmeno);
        if (postava == null) {
            return "Tato postava tu nen\u00ed!";
        }
        if (postava.getJmeno().equals("babka")  && aktualniProstor.getNazev().equals("domecek")) {
            aktualniProstor.vratSousedniProstor("les").vratSousedniProstor("jeskyne").jeVidet();
            return "Z lesa vede tajn\u00e1 stezka do jeskyn\u011b.\nV jeskyni najde\u0161 to, co hled\u00e1\u0161.\nAle pamatuj, bez dvou d\u016fle\u017eit\u00fdch v\u011bc\u00ed ve tm\u011b v\u00edt\u011bzstv\u00ed nevybojuje\u0161!";
        }
        if (postava.getJmeno().equals("kovar") && !this.plan.getInventar().obsahujeVec("mec") && aktualniProstor.getNazev().equals("kovarna")) {
            return "Tebe posílá král? Mám pro tebe meč, vem si ho z truhly.";
        }
        return postava.getDialog();
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}