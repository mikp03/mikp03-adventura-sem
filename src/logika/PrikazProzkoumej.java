package logika;/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */


/*******************************************************************************
 * Instance třídy logika.PrikazProzkoumej představují ...
 *
 * @author    jméno autora
 * @version   0.00.000
 */
public class PrikazProzkoumej implements IPrikaz
{
    private static final String NAZEV = "prozkoumej";
    private HerniPlan plan;
    

    /***************************************************************************
     *  Konstruktor tridy
     */
    PrikazProzkoumej(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Na dotaz vypíše popis aktuálního prostoru.
     * 
     * @return zpráva, kterou vypíše hra hráči
     */
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return plan.getAktualniProstor().dlouhyPopis() + "\n" + plan.getInventar().nazvyVeci();
        }
        final String jmeno = parametry[0];
        Vec vec = plan.getAktualniProstor().najdiVec(jmeno);
        if (vec == null || !vec.getNazev().equals("truhla")) {
            return "Tato věc nejde prokoumat!";
        }
        vec.prozkoumano(true);
        return vec.popisProzkoumej();
    }
    
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
      public String getNazev() {
        return NAZEV;
     }
}
