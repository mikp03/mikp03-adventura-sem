package main;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import logika.Hra;
import logika.IHra;
import uiText.TextoveRozhrani;
/**
 *  Class main.AdventuraZaklad - hlavní třída projektu.
 *
 *@author     Petr Mikan
 *@version    pro školní rok 2019/2020
 */
public class AdventuraZaklad extends Application {

    private TextArea centerTextArea;
    private IHra hra;
    private FlowPane dolniFlowPane;
    private VBox levyVBox;
    private VBox pravyVBox;
    private TextField prikazTextField;
    private OknoProstoru oknoProstoru;
    private PanelVychodu panelVychodu;
    private PanelInventare panelInventare;
    private MenuBar menuBar;
    private PanelVeciProstoru panelVeciProstoru;
    private BorderPane horniBorderPane;




    /***************************************************************************
     * Metoda, prostřednictvím níž se spouští celá aplikace.
     *
     * @param args Parametry příkazového řádku
     */

    public static void main(String[] args) {


        if (args.length == 0) {
            launch(args);
        } else {
            if (args[0].equals("-text")) {
                IHra hra = new Hra();
                TextoveRozhrani ui = new TextoveRozhrani(hra);
                ui.hraj();
                System.exit(0);
            } else {
                System.out.println("Neplatny parametr");
            }
        }

    }

    /***************************************************************************
     * Metoda, která zakládá grafické rozhraní a umístí jednotlivé části
     *
     * @param primaryStage primary stage
     */
    @Override
    public void start(Stage primaryStage) {

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent e) {
                Platform.exit();
                System.exit(0);
            }
        });

        BorderPane borderPane = new BorderPane();
        hra = new Hra();
        nastavTextArea();
        borderPane.setCenter(centerTextArea);
        nastavDolniPanel();
        borderPane.setBottom(dolniFlowPane);
        nastavLevyPanel();
        borderPane.setLeft(levyVBox);
        nastavHorniPanel();
        borderPane.setTop(horniBorderPane);
//        oknoProstoru = new OknoProstoru(hra.getHerniPlan());
//        borderPane.setTop(oknoProstoru.getPanel());

        panelVychodu = new PanelVychodu(hra.getHerniPlan());
        nastavPravyPanel();
        borderPane.setRight(pravyVBox);
        initMenu();
        VBox vBox = new VBox();
        vBox.getChildren().addAll(menuBar, borderPane);


//        borderPane.setCenter(new Label("Ahoj"));

        Scene scene = new Scene(vBox, 800, 650);
        primaryStage.setTitle("Adventura");
        primaryStage.setScene(scene);
        prikazTextField.requestFocus();
        primaryStage.show();
    }

    /**
     * Metoda zakládá horní panel s panelem nástrojů, obrázkem plánku hry a panelem věcí v prostoru
     */
    private void nastavHorniPanel() {
        horniBorderPane = new BorderPane();
        oknoProstoru = new OknoProstoru(hra.getHerniPlan());
        panelVeciProstoru = new PanelVeciProstoru(hra.getHerniPlan());
        ToolBar nastrojovaLista = new ToolBar();

        Button button1 = new Button("Nápověda");
        nastrojovaLista.getItems().add(button1);
        button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                String text = hra.zpracujPrikaz("napoveda");
                centerTextArea.appendText("\n" + "napoveda" + "\n");
                centerTextArea.appendText("\n" + text + "\n");
                prikazTextField.requestFocus();
            }
        });

        nastrojovaLista.getItems().add(new Separator());

        Button button2 = new Button("Prozkoumej");
        nastrojovaLista.getItems().add(button2);
        button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                String text = hra.zpracujPrikaz("prozkoumej");
                centerTextArea.appendText("\n" + "prozkoumej" + "\n");
                centerTextArea.appendText("\n" + text + "\n");
                prikazTextField.requestFocus();
            }
        });

        horniBorderPane.setTop(nastrojovaLista);
        horniBorderPane.setRight(panelVeciProstoru.getList());
        horniBorderPane.setLeft(oknoProstoru.getPanel());
    }

    /**
     * Metoda zakládá levý panel, kam VBOX s nadpisem a inventář
     */
    private void nastavLevyPanel() {
        levyVBox = new VBox();
        Label inventarLabel = new Label("Inventář");
        inventarLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        inventarLabel.setMinHeight(45);
        panelInventare = new PanelInventare(hra.getHerniPlan());

        levyVBox.getChildren().addAll(inventarLabel, panelInventare.getList());
        levyVBox.setAlignment(Pos.CENTER);
    }

    /**
     * Metoda zakládá pravý panel, kam umístí VBox s nadpisem a východy
     */
    private void nastavPravyPanel() {
        pravyVBox = new VBox();
        Label panelVychoduLabel = new Label("Východy");
        panelVychoduLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        panelVychoduLabel.setMinHeight(45);
        panelVychodu = new PanelVychodu(hra.getHerniPlan());

        pravyVBox.getChildren().addAll(panelVychoduLabel, panelVychodu.getList(),panelVychodu.getChoiceBox());
        pravyVBox.setAlignment(Pos.CENTER);



        panelVychodu.getChoiceBox().setOnAction(event -> {
            if (panelVychodu.getChoiceBox().getValue() != null) {
                String radek = "jdi " + panelVychodu.getChoiceBox().getValue();
                String text = hra.zpracujPrikaz(radek);
                centerTextArea.appendText("\n" + radek + "\n");
                centerTextArea.appendText("\n" + text + "\n");
                prikazTextField.requestFocus();
            }
        });
    }

    /**
     * Metoda zakládá dolní panel, kam umístí flow pane s nadpisem a textovým polem
     */
    private void nastavDolniPanel() {
        dolniFlowPane = new FlowPane();
        Label zadejPrikazLabel = new Label("Zadej prikaz ");
        zadejPrikazLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        prikazTextField = new TextField();
        prikazTextField.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                String radek = prikazTextField.getText();
                String text = hra.zpracujPrikaz(radek);
                centerTextArea.appendText("\n" + radek + "\n");
                centerTextArea.appendText("\n" + text + "\n");
                prikazTextField.setText("");
                if (hra.konecHry()) {
                    prikazTextField.setEditable(false);

                }
            }
        });

        dolniFlowPane.getChildren().addAll(zadejPrikazLabel, prikazTextField );
        dolniFlowPane.setAlignment(Pos.CENTER);
    }

    /**
     * Metoda nastaví textové pole
     */
    private void nastavTextArea() {
        centerTextArea = new TextArea();
        centerTextArea.setText(hra.vratUvitani());
        centerTextArea.setEditable(false);
    }

    /**
     * Metoda zakládá horní menu a jeho prvky.
     *
     */
    private void initMenu() {
        menuBar = new MenuBar();

        // --- Menu Soubor
        Menu menuSoubor = new Menu("Soubor");

        MenuItem novaHra = new MenuItem("Nová hra",
                new ImageView(new Image(AdventuraZaklad.class.getResourceAsStream("../zdroje/new.gif"))));
        novaHra.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                hra = new Hra();
                oknoProstoru.nastaveniHernihoPlanu(hra.getHerniPlan());
                panelVychodu.nastaveniHernihoPlanu(hra.getHerniPlan());
                panelInventare.nastaveniHernihoPlanu(hra.getHerniPlan());
                panelVeciProstoru.nastaveniHernihoPlanu(hra.getHerniPlan());
                centerTextArea.setText(hra.vratUvitani());
                prikazTextField.requestFocus();

            }
        });
        novaHra.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));


        MenuItem konec = new MenuItem("Konec");
        konec.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                System.exit(0);
            }
        });

        menuSoubor.getItems().addAll(novaHra, new SeparatorMenuItem(), konec);


        Menu menuNapoveda = new Menu("Nápověda");

        MenuItem napovedaKAplikaci = new MenuItem("Nápověda k aplikaci");
        napovedaKAplikaci.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                // obsluha události Nápověda k aplikaci
                // sekundární okno
                Stage stage = new Stage();
                stage.setTitle("Nápověda k aplikaci");
                WebView webview = new WebView();
                webview.getEngine().load(
                        AdventuraZaklad.class.getResource("/zdroje/napoveda.htm").toExternalForm()
                );
                stage.setScene(new Scene(webview, 500, 500));
                stage.show();
            }
        });

        MenuItem oProgramu = new MenuItem("O programu");
        oProgramu.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                // obsluha události O programu
                Alert alert = new Alert(Alert.AlertType.INFORMATION);


                alert.setTitle("Grafická adventura");
                alert.setHeaderText("JavaFX adventura");
                alert.setContentText("verze ZS 2019");
                alert.showAndWait();
            }
        });
        menuNapoveda.getItems().addAll(oProgramu, new SeparatorMenuItem(), napovedaKAplikaci);

        menuBar.getMenus().addAll(menuSoubor, menuNapoveda);
    }
}
