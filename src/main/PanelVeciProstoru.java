package main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import logika.HerniPlan;
import logika.Vec;
import util.ObserverZmenyInventare;
/**
 *  Class main.PanelVeciProstoru - vytvoří panel pro věci v prostoru.
 *
 *@author     Petr Mikan
 *@version    pro školní rok 2019/2020
 */
public class PanelVeciProstoru implements ObserverZmenyInventare {
    private HerniPlan plan;
    private ObservableList<String> data = FXCollections.observableArrayList();
    private ListView<String> list;

    /**
     *  Konstruktor třídy
     *
     *  @param plan herní plán, ve kterém se bude ve hře "chodit"
     */
    PanelVeciProstoru(HerniPlan plan) {
        this.plan = plan;
        plan.zaregistrujPozorovateleInventare(this);

        init();

    }

    /**
     * Metoda inicializuje grafický obsah věcí v prostoru
     */
    private void init() {
        list = new ListView<>();
        list.setItems(data);
        list.setPrefWidth(130);
        list.setMaxHeight(250);
        plan.zaregistrujPozorovateleInventare(this);

        list.setCellFactory((ListView<String> param) -> new ListCell<String>() {
            private ImageView imageView = new ImageView();

            @Override
            public void updateItem(String jmeno, boolean empty) {

                super.updateItem(jmeno, empty);

                if (empty) {

                    setText(null);
                    setGraphic(null);

                } else {
                    imageView.setImage(new Image("/zdroje/obrazkyVeci/" +jmeno+".jpg"));
                    setGraphic(imageView);

                }
            }
        });

        aktualizuj();
    }

    /**
     * Metoda vrací listview
     */
    ListView<String> getList() {
        return list;
    }

    /**
     * Metoda vyčístí obsah aktuálního prostoru a naplní aktuálními položkami.
     */
    @Override
    public void aktualizuj() {
        data.clear();
        for(Vec vec : plan.getAktualniProstor().seznamPredmetu()) {

            data.add(vec.getNazev());

        }
    }

    /**
     * Metoda zaregistruje pozorovatele k hernímu plánu při spuštění nové hry.
     * @param plan herni plán
     */
    void nastaveniHernihoPlanu(HerniPlan plan){
        this.plan = plan;
        plan.zaregistrujPozorovateleInventare(this);
        this.aktualizuj();
    }
}
