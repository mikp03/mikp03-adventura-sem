package main;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import logika.HerniPlan;
import util.ObserverZmenyProstoru;
/**
 *  Class main.OknoProstoru - vytvoří plánek prostoru.
 *
 *@author     Petr Mikan
 *@version    pro školní rok 2019/2020
 */
public class OknoProstoru implements ObserverZmenyProstoru {
    private HerniPlan plan;
    private AnchorPane horniAnchorPane;
    private Circle teckaCircle;

    /**
     *  Konstruktor třídy
     *
     *  @param plan herní plán, ve kterém se bude ve hře "chodit"
     */
    OknoProstoru(HerniPlan plan) {
        this.plan = plan;
        plan.zaregistrujPozorovatele(this);
        nastavHorniPanel();
    }

    /**
     * Metoda vrací horníAnchorPane
     */
    AnchorPane getPanel() {
        return horniAnchorPane;
    }

    /**
     * Metoda nastaví horní panel a to plánek
     */
    private void nastavHorniPanel() {
        horniAnchorPane = new AnchorPane();

        ImageView planekImageView = new ImageView(new Image(AdventuraZaklad.class.getResourceAsStream("../zdroje/planek.png"), 400, 250, false, false));

        teckaCircle = new Circle(10, Paint.valueOf("red"));

        AnchorPane.setTopAnchor(teckaCircle,plan.getAktualniProstor().getPosTop());
        AnchorPane.setLeftAnchor(teckaCircle, plan.getAktualniProstor().getPosLeft());

        aktualizuj();
        horniAnchorPane.getChildren().addAll(planekImageView, teckaCircle);
    }

    /**
     * Metoda nastaví aktuální souřadnice prostoru na plánku
     */
    @Override
    public void aktualizuj() {
        AnchorPane.setTopAnchor(teckaCircle,plan.getAktualniProstor().getPosTop());
        AnchorPane.setLeftAnchor(teckaCircle, plan.getAktualniProstor().getPosLeft());
    }

    /**
     * Metoda zaregistruje pozorovatele k hernímu plánu při spuštění nové hry.
     *
     * @param plan herní plán
     */
    void nastaveniHernihoPlanu (HerniPlan plan){
        this.plan = plan;
        plan.zaregistrujPozorovatele(this);
        this.aktualizuj();
    }
}
