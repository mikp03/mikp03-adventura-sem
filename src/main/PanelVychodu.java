package main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import logika.HerniPlan;
import logika.Prostor;
import util.ObserverZmenyProstoru;

/**
 *  Class main.PanelVychodu - vytvoří panel pro východy.
 *
 *@author     Petr Mikan
 *@version    pro školní rok 2019/2020
 */
public class PanelVychodu implements ObserverZmenyProstoru {
    private HerniPlan plan;
    private ListView<String> list;
    private ChoiceBox<String> choiceBox;
    private ObservableList<String> data;

    /**
     *  Konstruktor třídy
     *
     *  @param plan herní plán, ve kterém se bude ve hře "chodit"
     */
    PanelVychodu(HerniPlan plan) {
        this.plan = plan;
        plan.zaregistrujPozorovatele(this);
        init();

    }

    /**
     * Metoda inicializuje grafický obsah východů
     */
    private void init() {
        list = new ListView<>();
        choiceBox = new ChoiceBox<>();
        data = FXCollections.observableArrayList();
        list.setItems(data);
        list.setPrefWidth(130);
        choiceBox.setItems(data);

        aktualizuj();
    }

    /**
     * Metoda vrací Choice box
     */
    ChoiceBox getChoiceBox() {
        return choiceBox;
    }

    /**
     * Metoda vrací listview
     */
    ListView<String> getList() {
        return list;
    }

    @Override
    public void aktualizuj() {
        data.clear();
        for (Prostor vychod : plan.getAktualniProstor().getVychody()) {
            if (vychod.jeVidet()) {
                data.add(vychod.getNazev());

            }
        }
    }

    /**
     * Metoda zaregistruje pozorovatele k hernímu plánu při spuštění nové hry.
     * @param plan herni plán
     */
    void nastaveniHernihoPlanu(HerniPlan plan){
        this.plan = plan;
        plan.zaregistrujPozorovatele(this);
        this.aktualizuj();
    }
}