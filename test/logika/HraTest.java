package logika;

import org.junit.Test;
import org.junit.Assert;
import org.junit.After;
import org.junit.Before;

public class HraTest
{
    private Hra hra1;
    
    @Before
    public void setUp() {
        this.hra1 = new Hra();
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testPrubehHry() {
        this.hra1.zpracujPrikaz("mluv kral");
        this.hra1.zpracujPrikaz("jdi les");
        this.hra1.zpracujPrikaz("jdi domecek");
        Assert.assertEquals("domecek", this.hra1.getHerniPlan().getAktualniProstor().getNazev());
        Assert.assertNotNull(this.hra1.getHerniPlan().getAktualniProstor().najdiVec("postel"));
        Assert.assertNull(this.hra1.getHerniPlan().getAktualniProstor().najdiVec("hodiny"));
        this.hra1.zpracujPrikaz("seber postel");
        Assert.assertEquals(false, this.hra1.getHerniPlan().getInventar().obsahujeVec("postel"));
        Assert.assertNotNull(this.hra1.getHerniPlan().getAktualniProstor().najdiVec("postel"));
        this.hra1.zpracujPrikaz("mluv babka");
        this.hra1.zpracujPrikaz("jdi les");
        this.hra1.zpracujPrikaz("seber vetev");
        Assert.assertEquals(true, this.hra1.getHerniPlan().getInventar().obsahujeVec("vetev"));
    }
    
    @Test
    public void testVyhra() {
        this.hra1.zpracujPrikaz("jdi les");
        this.hra1.zpracujPrikaz("seber vetev");
        this.hra1.zpracujPrikaz("jdi kovarna");
        this.hra1.zpracujPrikaz("mluv kovar");
        this.hra1.zpracujPrikaz("dej kovar vetev");
        this.hra1.zpracujPrikaz("jdi les");
        this.hra1.zpracujPrikaz("seber koreny");
        this.hra1.zpracujPrikaz("jdi domecek");
        this.hra1.zpracujPrikaz("jdi louka");
        this.hra1.zpracujPrikaz("seber kviti");
        this.hra1.zpracujPrikaz("jdi domecek");
        this.hra1.zpracujPrikaz("dej babka kviti");
        this.hra1.zpracujPrikaz("jdi les");
        this.hra1.zpracujPrikaz("jdi jeskyne");
        this.hra1.zpracujPrikaz("bojuj bandita");
        this.hra1.zpracujPrikaz("jdi jeskynepoklad");
        this.hra1.zpracujPrikaz("seber poklad");
        Assert.assertEquals(true, this.hra1.konecHry());
    }
    
    @Test
    public void testProhra() {
        this.hra1.zpracujPrikaz("jdi les");
        this.hra1.zpracujPrikaz("seber vetev");
        this.hra1.zpracujPrikaz("jdi kovarna");
        this.hra1.zpracujPrikaz("mluv kovar");
        this.hra1.zpracujPrikaz("jdi les");
        this.hra1.zpracujPrikaz("seber koreny");
        this.hra1.zpracujPrikaz("jdi domecek");
        this.hra1.zpracujPrikaz("jdi louka");
        this.hra1.zpracujPrikaz("seber kviti");
        this.hra1.zpracujPrikaz("jdi domecek");
        this.hra1.zpracujPrikaz("dej babka kviti");
        this.hra1.zpracujPrikaz("jdi les");
        this.hra1.zpracujPrikaz("jdi jeskyne");
        this.hra1.zpracujPrikaz("bojuj bandita");
        Assert.assertEquals(true, this.hra1.konecHry());
    }
    
    @Test
    public void testProhra2() {
        this.hra1.zpracujPrikaz("jdi les");
        this.hra1.zpracujPrikaz("seber vetev");
        this.hra1.zpracujPrikaz("jdi kovarna");
        this.hra1.zpracujPrikaz("dej kovar vetev");
        this.hra1.zpracujPrikaz("jdi les");
        this.hra1.zpracujPrikaz("seber koreny");
        this.hra1.zpracujPrikaz("jdi domecek");
        this.hra1.zpracujPrikaz("jdi louka");
        this.hra1.zpracujPrikaz("seber kviti");
        this.hra1.zpracujPrikaz("jdi domecek");
        this.hra1.zpracujPrikaz("dej babka kviti");
        this.hra1.zpracujPrikaz("jdi les");
        this.hra1.zpracujPrikaz("jdi jeskyne");
        this.hra1.zpracujPrikaz("bojuj bandita");
        Assert.assertEquals(true, this.hra1.konecHry());
    }
    
    @Test
    public void testProhra3() {
        this.hra1.zpracujPrikaz("jdi les");
        this.hra1.zpracujPrikaz("seber vetev");
        this.hra1.zpracujPrikaz("jdi les");
        this.hra1.zpracujPrikaz("seber koreny");
        this.hra1.zpracujPrikaz("jdi domecek");
        this.hra1.zpracujPrikaz("jdi louka");
        this.hra1.zpracujPrikaz("seber kviti");
        this.hra1.zpracujPrikaz("jdi domecek");
        this.hra1.zpracujPrikaz("dej babka kviti");
        this.hra1.zpracujPrikaz("jdi les");
        this.hra1.zpracujPrikaz("jdi jeskyne");
        this.hra1.zpracujPrikaz("bojuj bandita");
        Assert.assertEquals(true, this.hra1.konecHry());
    }
}