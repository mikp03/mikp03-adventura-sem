package logika;

import org.junit.Test;
import org.junit.Assert;
import org.junit.After;
import org.junit.Before;

//
// Decompiled by Procyon v0.5.36
//

public class ProstorTest
{
    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testLzeProjit() {
        final Prostor prostor1 = new Prostor("hala", "vstupn\u00ed hala budovy V\u0160E na Ji\u017en\u00edm m\u011bst\u011b", 100, 100);
        final Prostor prostor2 = new Prostor("bufet", "bufet, kam si m\u016f\u017eete zaj\u00edt na sva\u010dinku",100, 100);
        prostor1.setVychod(prostor2);
        prostor2.setVychod(prostor1);
        Assert.assertEquals(prostor2, prostor1.vratSousedniProstor("bufet"));
        Assert.assertNull(prostor2.vratSousedniProstor("pokoj"));
    }

    @Test
    public void testVeci() {
        final Prostor prostor1 = new Prostor("a", null, 100, 100);
        final Vec vec1 = new Vec("a", true);
        final Vec vec2 = new Vec("b", true);
        Assert.assertTrue(prostor1.vlozVec(vec1));
        Assert.assertTrue(prostor1.vlozVec(vec2));
        prostor1.odeberVec("a");
        Assert.assertNotNull(prostor1.najdiVec("a"));
    }

    @Test
    public void testPostav() {
        final Prostor prostor1 = new Prostor("a", null, 100, 100);
        final Postava postava1 = new Postava("Honza", "bla");
        final Postava postava2 = new Postava("Filip", "blabla");
        Assert.assertTrue(prostor1.vlozPostavu(postava1));
        Assert.assertTrue(prostor1.vlozPostavu(postava2));
        Assert.assertNotNull(prostor1.odeberPostavu("Honza"));
    }
}