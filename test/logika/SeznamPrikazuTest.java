package logika;

import logika.*;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída logika.SeznamPrikazuTest slouží ke komplexnímu otestování třídy
 * logika.SeznamPrikazu
 * 
 * @author    Luboš Pavlíček, Petr Mikan
 * @version   pro školní rok 2017/2018
 */
public class SeznamPrikazuTest
{
    private Hra hra;
    private PrikazKonec prKonec;
    private PrikazJdi prJdi;
    private PrikazSeber prSeber;
    private PrikazMluv prMluv;
    private PrikazBojuj prBojuj;
    private PrikazDej prDej;
    private PrikazProzkoumej prProzkoumej;
    
    /**
     * Před každým spuštěním testovací metody vytvoří objekty, které se budou testovat.
     */
    @Before
    public void setUp() {
        hra = new Hra();
        prKonec = new PrikazKonec(hra);
        prJdi = new PrikazJdi(hra.getHerniPlan());
        prSeber = new PrikazSeber (hra.getHerniPlan());
        prMluv = new PrikazMluv (hra.getHerniPlan());
        prBojuj = new PrikazBojuj (hra.getHerniPlan());
        prDej = new PrikazDej (hra.getHerniPlan());
        prProzkoumej = new PrikazProzkoumej (hra.getHerniPlan());
    }
    
    /**
     * Testuje vložení příkazů.
     */
    @Test
    public void testVlozeniVybrani() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prMluv);
        seznPrikazu.vlozPrikaz(prBojuj);
        seznPrikazu.vlozPrikaz(prDej);
        seznPrikazu.vlozPrikaz(prProzkoumej);
        assertEquals(prKonec, seznPrikazu.vratPrikaz("konec"));
        assertEquals(prJdi, seznPrikazu.vratPrikaz("jdi"));
        assertEquals(null, seznPrikazu.vratPrikaz("napoveda"));
        assertEquals(prSeber, seznPrikazu.vratPrikaz("seber"));
        assertEquals(prMluv, seznPrikazu.vratPrikaz("mluv"));
        assertEquals(prBojuj, seznPrikazu.vratPrikaz("bojuj"));
        assertEquals(prDej, seznPrikazu.vratPrikaz("dej"));
        assertEquals(prProzkoumej, seznPrikazu.vratPrikaz("prozkoumej"));
    }
    
    /**
     * Testuje platnost příkazů.
     */
    @Test
    public void testJePlatnyPrikaz() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prMluv);
        seznPrikazu.vlozPrikaz(prBojuj);
        seznPrikazu.vlozPrikaz(prDej);
        seznPrikazu.vlozPrikaz(prProzkoumej);
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("konec"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("jdi"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("napoveda"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("Konec"));
    }
    
    /**
     * Testuje názvy příkazů.
     */
    @Test
    public void testNazvyPrikazu() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        String nazvy = seznPrikazu.vratNazvyPrikazu();
        assertEquals(true, nazvy.contains("konec"));
        assertEquals(true, nazvy.contains("jdi"));
        assertEquals(false, nazvy.contains("napoveda"));
        assertEquals(false, nazvy.contains("Konec"));
    }
    
}
